import paho.mqtt.client as mqtt
import json
import time

# MQTT Broker settings
broker_address = "localhost"  # Replace with your MQTT broker's address
port = 1886  # Default MQTT port
service_topic = "thing/product/SimDock/services"
reply_topic = "thing/product/SimDock/requests_reply"
username = ""  # Replace with your MQTT broker username
password = ""  # Replace with your MQTT broker password

# Messages
message1 = {"method": "flighttask_prepare", "bid": "55cbdd32-88b5-4b47-bc49-a559e25284d7", "tid": "58c6669c-c8a7-45d5-86b7-edc729d37f5d", "timestamp": 1698314362000, "data": {"execute_time": 1698314362000, "flight_id": "37120de2-28ef-4318-a301-5fb474e44507", "task_type": 0, "wayline_type": 0, "file": {"url": "https://drive.google.com/uc?export=download&id=1V6-idSVuwLmsv42TYy_dVOLD9ZJOSfPT", "fingerprint": "d318b25d369ef23d65de9748ca06aebb"}, "rth_altitude": 30, "out_of_control_action": 0}}

message2 = {
    "method": "flighttask_execute",
    "bid": "307a20fb-bc62-49d4-a8ac-ed6795a60ea6",
    "tid": "968df989-4154-4cbc-9eb2-be68733c7ef0",
    "timestamp": 1698307354000,
    "data": {
        "flight_id": "156831c0-6955-4dd7-a176-ba70494c83bf"
    }
}
message3 = {"tid": "8ae2c52d-2586-4ca4-9a78-eac36a00144d", "bid": "e26091a6-b015-4c33-ad03-7b72aa05de3c", "method": "flighttask_resource_get", "timestamp": 1698314366000, "data": {"result": 0, "output": {"file": {"url": "https://drive.google.com/uc?export=download&id=1V6-idSVuwLmsv42TYy_dVOLD9ZJOSfPT", "fingerprint": "d318b25d369ef23d65de9748ca06aebb"}}}}

# Create an MQTT client instance
client = mqtt.Client()
#client.tls_set()

# Set the username and password
client.username_pw_set(username, password)

# Connect to the MQTT broker
client.connect(broker_address, port, 60)

# Publish message1 to the service topic
message_json = json.dumps(message1)
client.publish(service_topic, message_json)
print(f"Published: {message_json}")
time.sleep(2)  # Wait for 2 seconds

# Publish message2 to the service topic
message_json = json.dumps(message2)
client.publish(service_topic, message_json)
print(f"Published: {message_json}")
time.sleep(2)  # Wait for 2 seconds

# Publish message3 to the reply topic
message_json = json.dumps(message3)
client.publish(reply_topic, message_json)
print(f"Published: {message_json}")

# Disconnect from the MQTT broker
client.disconnect()
