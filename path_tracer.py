import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import paho.mqtt.client as mqtt
import json

# Global variables for storing coordinates
coordinates = {'x': [], 'y': []}

# Function to update plot in real-time with received coordinates
# Function to update plot in real-time with received coordinates
limits_set = False

# Function to update plot in real-time with received coordinates
# Function to update plot in real-time with received coordinates
def update_plot(latitude, longitude):
    # Ignore coordinates with values of 0, 0
    if latitude == 0 and longitude == 0:
        return
    
    coordinates['x'].append(longitude)
    coordinates['y'].append(latitude)
    
    # Filter out coordinates with values of 0, 0
    filtered_x = [x for x, y in zip(coordinates['x'], coordinates['y']) if y != 0 or x != 0]
    filtered_y = [y for x, y in zip(coordinates['x'], coordinates['y']) if y != 0 or x != 0]
    
    plt.plot(filtered_x, filtered_y, marker='o', color='b')  # Plot the coordinates
    plt.plot(filtered_x, filtered_y, color='r')  # Plot the path
    
    # Adjust plot window size based on the range of coordinates
    margin = 0.001  # Margin to add around the limits
    min_x = min(filtered_x) - margin
    max_x = max(filtered_x) + margin
    min_y = min(filtered_y) - margin
    max_y = max(filtered_y) + margin
    plt.xlim(min_x, max_x)
    plt.ylim(min_y, max_y)



# MQTT Broker settings
broker_address = "localhost"  # Replace with your MQTT broker's address
port = 1886  # Default MQTT port
topic = "thing/product/SimDrone/osd"  # Replace with the MQTT topic you want to subscribe to
username = ""  # Replace with your MQTT broker username
password = ""  # Replace with your MQTT broker password

# Callback when a message is received on the subscribed topic
def on_message(client, userdata, message):
    try:
        received_message = json.loads(message.payload.decode("utf-8"))
        latitude = received_message["data"]["latitude"]
        longitude = received_message["data"]["longitude"]
        print('Latitude:', latitude)
        print('Longitude:', longitude)
        update_plot(latitude, longitude)
    except Exception as e:
        print("Error:", e)		

# Create an MQTT client instance
client = mqtt.Client()

# Set the callback functions
client.on_message = on_message

# Set the username and password
client.username_pw_set(username, password)

# Connect to the MQTT broker
client.connect(broker_address, port, 60)

# Subscribe to the MQTT topic
client.subscribe(topic)

# Initialize the plot
plt.figure(figsize=(8, 6))  # Set initial figure size
plt.grid(True)

# Function to update the animation with each incoming message
def update(frame):
    pass  # The animation will be updated in the update_plot function

# Start the animation
ani = FuncAnimation(plt.gcf(), update, interval=1000)  # Update plot every second

# Start the MQTT client's network loop to receive messages
client.loop_start()

# Keep the plot open
plt.show()
